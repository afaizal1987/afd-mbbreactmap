import React, { useState, useEffect, useRef } from "react";
import Map from './Map'
let autoComplete;

/**
 * using Google API javascript
 * no library
 * can be changed to use libraries
 * Tutorial by Gapur Kassym
 */
const loadScript = (url, callback) => {
  let script = document.createElement("script");
  script.type = "text/javascript";

  if (script.readyState) {
    // Once script is loaded
    script.onreadystatechange = function() {
      if (script.readyState === "loaded" || script.readyState === "complete") {
        script.onreadystatechange = null;
        callback();
      }
    };
  } else {
    // Awaiting callback
    script.onload = () => callback();
  }

  script.src = url;
  // Setting this script on the head section of HTML
  document.getElementsByTagName("head")[0].appendChild(script);
};

// Handling the load when the script is calling
function handleScriptLoad(updateQuery, autoCompleteRef) {
  
  autoComplete = new window.google.maps.places.Autocomplete(
    autoCompleteRef.current,
    {
       types: ["(cities)"] 
    }
  );
  // Setting the fields to accept addresses
  autoComplete.setFields(
    ["address_components", "formatted_address"]
  );
  // Listen to change
  autoComplete.addListener("place_changed", () =>
    handlePlaceSelect(updateQuery)
  );
}

// If there is a place change, update the query
async function handlePlaceSelect(updateQuery) {
  const addressObject = autoComplete.getPlace();
  const query = addressObject.formatted_address;
  updateQuery(query);
  return query
}

function SearchLocal() {
  // Initialising the query to null
  const [query, setQuery] = useState("");

  // Emptying the reference
  const autoCompleteRef = useRef(null);



  // When there is a change, load the google maps script
  useEffect(() => {
    loadScript(
      `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_MAP}&libraries=places`,
      () => handleScriptLoad(setQuery, autoCompleteRef)
    );
  }, []);

  return (
    <div>
      <input
        ref={autoCompleteRef}
        onChange={event => setQuery(event.target.value)}
        placeholder="Enter City"
        value={query}
      />
    </div>
  );
}

export default SearchLocal;
